----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:54:09 11/04/2013 
-- Design Name: 
-- Module Name:    main - Behavioral qwertyuiop
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
    Port(	clk : in  STD_LOGIC;
				reset : in  STD_LOGIC;
				leds : out  std_logic_vector(7 downto 0));
			
end main;

architecture Behavioral of main is

	signal cnt_led : std_logic_vector(31 downto 0);
	constant c_end : std_logic_vector(31 downto 0) := x"006940AA"; -- 03f9400a --7940aa
	signal shift_reg : std_logic_vector(7 downto 0);
	signal direction : std_logic;

begin
	
	p_cnt : process(clk, reset)
	begin
		if(reset = '0') then
			cnt_led <= (others => '0');
		elsif(clk'event and clk = '1') then
			if(cnt_led < c_end) then
				cnt_led <= cnt_led + 1;
			else
				cnt_led <= (others => '0');
			end if;
		end if;
	end process;
	
	p_led : process(clk, reset)
	begin
		if(reset = '0') then
			shift_reg <= "10000000";
			direction <= '1';
		elsif(clk'event and clk = '1') then
			if (cnt_led = c_end) then
				if(direction = '1') then											-- Direction = 1 => Desplazamos hacia la derecha
					shift_reg <= shift_reg(0) & shift_reg(7 downto 1);
					if shift_reg = "00000010" then
						direction <= '0';
					end if;
				else																		-- Direction = 0 => Desplazamos hacia la izquierda
					shift_reg <= shift_reg(6 downto 0) & shift_reg(7);
					if shift_reg = "01000000" then
						direction <= '1';
					end if;
				end if;
			end if;
		end if;
	end process;

	leds <= shift_reg;

end Behavioral;

